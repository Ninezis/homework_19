#include <iostream>

using namespace std;

class Animal
{
public:
	virtual void Voice()
	{
		cout << "sound";
	}
	virtual ~Animal(){}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		cout << "Dog say: \n";
	}
	~Dog()
	{
		cout << "Woof! \n";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << "Cat say: \n";
	}
	~Cat()
	{
		cout << "Meaw! \n";
	}
};

class Fox :public Animal
{
public:
	void Voice() override
	{
		cout << "Fox say: \n";
	}
	~Fox() 
	{
		cout << "What the fox say? \n";
	}
};

int main()
{
	Animal* animals[3];
	animals[0] = new Dog();
	animals[1] = new Cat();
	animals[2] = new Fox();

	for (Animal* a:animals)
	{
		a->Voice();

		delete a;
	}
	
	
	return 0;
}